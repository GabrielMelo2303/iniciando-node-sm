let express = require('express');
let app = express();
const port = 8080;

// Habilitando o serviço na porta 8080 con
app.listen(port, function () {
    console.log("Projeto rodando na porta: " + port);
})

// .get = método a ser feito , request = requisição da api , response= resposta do request.
// dentro da {} significa a funçao que vai er feita no momento em que a api for acionada

//recurso principal
app.get('/', (request, response) => {
    //response.send envia a resposta de volta a pessoa que fez a requisição
    response.send("{message: método get}");
});

//recurso funcionario
// dentro do meu app.get('<aqui dentro vai o CAMINHO onde minha requisição será requisitada>')
app.get('/funcionario', (request, response) => {
    let obj = request.query;
    let nome = obj.nome;
    let sobrenome = obj.sobrenome;

    response.send("{message: método funcionario" + nome  + sobrenome + "}" );
    //localhost:8080/funcionario/?nome=Gabriel&sobrenome=Melo
})